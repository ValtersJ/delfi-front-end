
function renderErrors(errors) {
	$("#errors").empty();	//old #errors content should be deleted
	for (var i=0; i < errors.length; i++) {

		$("#errors").append("<div> &raquo; " + errors[i] + "</div>").show();
	};
}

function validateForm() {
	result = true;
	var errors = [];


	if ($("#full-name").val() == '') {
		errors.push("Lauks 'Vārds, Uzvārds' ir jānorāda obligāti");
		result = false;
	}

	var tel = $("#telnr").val();
	var format =/\b\d{8}\b/;

	if (tel == '') {
		errors.push("Lauks 'Telefona numurs' ir jānorāda obligāti");
		result = false;

	}else if (!format.test(tel)){
		errors.push("Lauks 'Telefona numurs' ir norādīts nepareizi");
		result = false;
	}

	if ($("#message").val() == '') {
		errors.push("Lauks 'Ziņojums' ir jānorāda obligāti");
		result = false;
	}

	if (errors[0] == undefined) {
		return true;
	} else {
		renderErrors(errors);
		return false;
	}
}

window.onload = function() {
	null;
}