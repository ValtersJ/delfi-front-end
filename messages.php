<!DOCTYPE html>
<html>
<head>
	<title>Read</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="base.css" type="text/css" />

</head>
<body>
	<div class="container" id="table-wrapper">
		<h1 id="heading">Formu dati</h1>

		<?php
			include_once 'phps.php';
						//outputs form
			$form = new OutputSingleForm();
			$form->showForms();			
		?>
		<footer>
			<a href="index.html">Uz formu</a>
		</footer>
	</div>
</body>
</html>

