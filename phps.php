<?php
    //Creates connection to database
class DatabaseConnection
{
	private $server;
	private $userName;
	private $password;
	private $databaseName;

	protected function connectToDatabase(){
		$this->server = "localhost";
		$this->userName = "root";
		$this->password = "";
		$this->databaseName = "post";

		$connection = new mysqli($this->server, $this->userName, $this->password, $this->databaseName);
		if (mysqli_connect_errno()) {
		    printf("Connect failed: %s\n", mysqli_connect_error());
		    exit();
		}else{
		return $connection;
		}
	}

}

class FormOperations extends DatabaseConnection
	{

		//adds Form to database
		public function addForm($arr){

			$fullName = $arr['fullName'];
			$telNr = $arr['telnr'];
			$address = $arr['address'];
			$message = $arr['message'];

			$sql = "INSERT INTO post_table(`ID`, `FullName`, `Tel`, `Address`, `Message`) VALUES ('0','$fullName','$telNr','$address','$message')";
			$this->connectToDatabase()->query($sql);
			
			header("Location: index.html");
		}

		//gets all Data from post_table
	protected function getForms(){
		$sql = "SELECT * FROM post_table";

		$answer = $this->connectToDatabase()->query($sql);

		$rows = $answer->num_rows;
		if($rows > 0){
			while ($row = $answer->fetch_assoc()) {
				$allData[] = $row;
			}
			return $allData;
		}
	}

	}

class OutputSingleForm extends FormOperations
{
	//outputs single form

	public function showForms(){
		$datas = $this->getForms();
		if (is_array($datas)){
			foreach ($datas as $data) {
				echo "<div class = 'row'>";
					echo "<div class = 'col col-md-2'>".$data['FullName']."</div>";
					echo "<div class = 'col col-md-2'>".$data['Tel']."</div>";
					echo "<div class = 'col col-md-2'>".$data['Address']."</div>";
					echo "<p class = 'col'>".$data['Message']."</p>";
				echo "</div>";
				echo "<hr>";

			}

		}
	}
}
